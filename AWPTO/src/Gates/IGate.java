package Gates;

public interface IGate {
    boolean function(boolean a);
    boolean function(boolean a, boolean b);
}
