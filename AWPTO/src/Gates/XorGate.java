package Gates;

public class XorGate implements IGate {
    @Override
    public boolean function(boolean a) {
        return false;
    }

    @Override
    public boolean function(boolean a, boolean b) {
        return ((a||b)&&!(a&&b));
    }
}
