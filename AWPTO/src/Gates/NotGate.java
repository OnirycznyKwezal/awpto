package Gates;

public class NotGate implements IGate {

    @Override
    public boolean function(boolean a) {
        return !a;
    }

    @Override
    public boolean function(boolean a, boolean b) {
        return false;
    }
}
