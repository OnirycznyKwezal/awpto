package HelpersTests;

import Helpers.Converter;
import org.junit.Assert;
import org.junit.Test;

public class ConverterTest {
    private Converter converter = new Converter();

    @Test
    public void convertTest() {
        boolean[][] expected = {{false, true}};

        Assert.assertArrayEquals(expected, converter.convert(1));
    }

    @Test
    public void convertTest2() {
        boolean[][] expected = {{false, false, true, true}, {false, true, false, true}};
        Assert.assertArrayEquals(expected, converter.convert(2));
    }

    @Test
    public void intToBinTest() {
        Assert.assertEquals("00000101", converter.intToBin(5, 8));
    }

    @Test
    public void intToBinTest2() {
        Assert.assertEquals("01010000", converter.intToBin(5200, 8));
    }
}
