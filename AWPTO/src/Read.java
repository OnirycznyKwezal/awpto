import Helpers.Converter;
import Helpers.Lexer;
import Helpers.Parser;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Read {
    public static void main(String[] args) throws IOException {

        Lexer lexer;
        Converter convert;
        Parser parser;
        File file = new File("src/test.txt");
        Scanner sc = new Scanner(file);

        Converter converter = new Converter();
        converter.convert(2);

        String input;
        while (sc.hasNextLine()){
            input=sc.nextLine();
            System.out.println(input);
            System.out.println();
            lexer = new Lexer(input);
            convert = new Converter(lexer.getSymbolsNumber());
            parser = new Parser(lexer.getTokens(), convert.getTable());
            parser.truthTable();
            System.out.println();
        }
    }
}
