package GatesTest;

import Gates.OrGate;
import org.junit.Assert;
import org.junit.Test;

public class OrGateTest {
    private OrGate orGate = new OrGate();

    @Test
    public void Test1(){
       Assert.assertEquals(true, orGate.function(true, true));
    }

    @Test
    public void Test2(){
        Assert.assertEquals(true, orGate.function(true, false));
    }

    @Test
    public void Test3(){
        Assert.assertEquals(true, orGate.function(false, true));
    }

    @Test
    public void Test4(){
        Assert.assertEquals(false, orGate.function(false, false));
    }
}
