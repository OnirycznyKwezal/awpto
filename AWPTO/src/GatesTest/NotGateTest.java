package GatesTest;

import Gates.NotGate;
import org.junit.Assert;
import org.junit.Test;

public class NotGateTest {
    private NotGate notGate = new NotGate();

    @Test
    public void Test1(){
       Assert.assertEquals(false, notGate.function(true));
    }

    @Test
    public void Test2(){
        Assert.assertEquals(true, notGate.function(false));
    }
}
