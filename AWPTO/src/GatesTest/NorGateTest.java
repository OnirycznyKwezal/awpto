package GatesTest;

import Gates.NorGate;
import org.junit.Assert;
import org.junit.Test;

public class NorGateTest {
    private NorGate norGate = new NorGate();

    @Test
    public void Test1(){
       Assert.assertEquals(false, norGate.function(true, true));
    }

    @Test
    public void Test2(){
        Assert.assertEquals(false, norGate.function(true, false));
    }

    @Test
    public void Test3(){
        Assert.assertEquals(false, norGate.function(false, true));
    }

    @Test
    public void Test4(){
        Assert.assertEquals(true, norGate.function(false, false));
    }
}
