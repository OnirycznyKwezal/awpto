package GatesTest;

import Gates.XorGate;
import org.junit.Assert;
import org.junit.Test;

public class XorGateTest {
    private XorGate xorGate = new XorGate();

    @Test
    public void Test1(){
       Assert.assertEquals(false, xorGate.function(false, false));
    }

    @Test
    public void Test2(){
        Assert.assertEquals(true, xorGate.function(true, false));
    }

    @Test
    public void Test3(){
        Assert.assertEquals(true, xorGate.function(false, true));
    }

    @Test
    public void Test4(){
        Assert.assertEquals(false, xorGate.function(true, true));
    }
}
