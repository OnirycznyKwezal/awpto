package GatesTest;

import Gates.XnorGate;
import org.junit.Assert;
import org.junit.Test;

public class XnorGateTest {
    private XnorGate xnorGate = new XnorGate();

    @Test
    public void Test1(){
       Assert.assertEquals(true, xnorGate.function(true, true));
    }

    @Test
    public void Test2(){
        Assert.assertEquals(false, xnorGate.function(true, false));
    }

    @Test
    public void Test3(){
        Assert.assertEquals(false, xnorGate.function(false, true));
    }

    @Test
    public void Test4(){
        Assert.assertEquals(true, xnorGate.function(false, false));
    }
}
