package GatesTest;

import Gates.AndIGate;
import org.junit.Assert;
import org.junit.Test;

public class AndGateTest {
    private AndIGate andGate = new AndIGate();

    @Test
    public void Test1(){
       Assert.assertEquals(true, andGate.function(true, true));
    }

    @Test
    public void Test2(){
        Assert.assertEquals(false, andGate.function(true, false));
    }

    @Test
    public void Test3(){
        Assert.assertEquals(false, andGate.function(false, true));
    }

    @Test
    public void Test4(){
        Assert.assertEquals(false, andGate.function(false, false));
    }
}
