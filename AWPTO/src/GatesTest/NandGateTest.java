package GatesTest;

import Gates.NandGate;
import org.junit.Assert;
import org.junit.Test;

public class NandGateTest {
    private NandGate nandGate = new NandGate();

    @Test
    public void Test1(){
       Assert.assertEquals(false, nandGate.function(true, true));
    }

    @Test
    public void Test2(){
        Assert.assertEquals(true, nandGate.function(true, false));
    }

    @Test
    public void Test3(){
        Assert.assertEquals(true, nandGate.function(false, true));
    }

    @Test
    public void Test4(){
        Assert.assertEquals(true, nandGate.function(false, false));
    }
}
