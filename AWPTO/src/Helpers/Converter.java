package Helpers;

public class Converter {
    private boolean[][] table;

    public Converter(int number) {
        table = convert(number);
    }

    public Converter() {}

    public boolean[][] convert(int number) {
        boolean[][] result = new boolean[number][(int) Math.pow((double) 2, (double) number)];

        String binary;

        for (int i = 0; i < (int) Math.pow((double) 2, (double) number); i++) {
            binary = intToBin(i, number);
            for (int j = 0; j < number; j++) {
                if (binary.charAt(j) == '1') result[j][i] = true;
                else result[j][i] = false;
            }
        }

        return result;
    }

    public void display() {
        for (int i = 0; i < table[0].length; i++) {
            for (int j = 0; j < table.length; j++) {
                System.out.print(table[j][i]);
            }
            System.out.println();
        }
    }

    public boolean[][] getTable() {
        return table;
    }

    public String intToBin(int number, int digits) {
        StringBuilder str = new StringBuilder();
        for (int a = 0; a < digits; a++) {
            if (number % 2 == 1) {
                str.insert(0, "1");
            }
            if (number % 2 == 0) {
                str.insert(0, "0");
            }
            number = number / 2;
        }
        return str.toString();
    }
}
