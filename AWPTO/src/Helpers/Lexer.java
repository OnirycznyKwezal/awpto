package Helpers;

import java.util.ArrayList;

public class Lexer {
    private ArrayList<Token> tokens = new ArrayList<>();
    private int symbols = 0;

    public Lexer(String input) {
        int indicator = 0;

        while (indicator < input.length()) {
            if (Character.isWhitespace(input.charAt(indicator))) {
                indicator++;
            } else if (Character.isAlphabetic(input.charAt(indicator))) {
                tokens.add(new Token("SYMBOL", String.valueOf(input.charAt(indicator))));
                symbols++;
                indicator++;
            } else if (input.charAt(indicator) == '(') {
                tokens.add(new Token("LP", "("));
                indicator++;
            } else if (input.charAt(indicator) == ')') {
                tokens.add(new Token("RP", ")"));
                indicator++;
            } else if (input.charAt(indicator) == '|') {
                tokens.add(new Token("GATE", "Gates.OrGate"));
                indicator++;
            } else if (input.charAt(indicator) == '&') {
                tokens.add(new Token("GATE", "AND"));
                indicator++;
            } else if (input.charAt(indicator) == '*') {
                tokens.add(new Token("GATE", "Gates.XorGate"));
                indicator++;
            }else if (input.charAt(indicator) == '~') {
                if(Character.isAlphabetic(input.charAt(indicator +1))) tokens.add(new Token("GATE", "Gates.NotGate"));
                else if (input.charAt(indicator +1) == '|') {
                    tokens.add(new Token("GATE", "Gates.NorGate"));
                    indicator++;
                } else if (input.charAt(indicator +1) == '&') {
                    tokens.add(new Token("GATE", "Gates.NandGate"));
                    indicator++;
                }
                    indicator++;
                }
        }
    }

    public ArrayList<Token> getTokens() {
        return tokens;
    }

    public int getSymbolsNumber() {
        return symbols;
    }

    void display(){
        for (Token token : tokens) {
            System.out.println("Nazwa: " + token.getName() + " wartosc: " + token.getValue());
        }
    }
}
