package Helpers;

import Gates.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Parser {
    private int counter;
    private boolean[][] table;
    private ArrayList<Token> tokens;
    private List<String> symbolsList;
    private IGate nand, nor, and, or, xor, not;

    public Parser(ArrayList<Token> tokens, boolean[][] table) {
        this.tokens = tokens;
        this.table = table;

        nand = new NandGate();
        nor = new NorGate();
        and = new AndIGate();
        or = new OrGate();
        xor = new XorGate();
        not = new NotGate();
        counter = 0;
        symbolsList = new ArrayList<>();

        for (Token token : tokens) {
            if (token.getName().equals("SYMBOL")) symbolsList.add(token.getValue());
        }

        symbolsList = symbolsList.stream().distinct().collect(Collectors.toList());
    }

    public void truthTable() {
        for (String aSymbolsList : symbolsList) System.out.print(aSymbolsList + " ");
        System.out.println("| x");
        for (int i = 0; i < symbolsList.size(); i++) System.out.print("--");
        System.out.println("---");
        for (int i = 0; i < table[0].length; i++) {
            counter = 0;
            for (boolean[] aTable : table) {
                System.out.print(parseBoolean(aTable[i]) + " ");
            }
            System.out.println("| " + parseBoolean(parse(i)));
        }
    }

    public boolean parse(int line) {
        boolean value = false;
        boolean unaryValue = false;
        while (counter < tokens.size()) {
            if (tokens.get(counter).getName().equals("SYMBOL"))
                value = symbolToBool(tokens.get(counter).getValue(), line);
            else if (tokens.get(counter).getName().equals("GATE")) {
                if (tokens.get(counter).getValue().equals("Gates.NandGate")) {
                    counter++;
                    if (tokens.get(counter).getValue().equals("Gates.NotGate")) {
                        counter++;
                        if (tokens.get(counter).getName().equals("SYMBOL"))
                            unaryValue = not.function(symbolToBool(tokens.get(counter).getValue(), line));
                        else if (tokens.get(counter).getName().equals("LP")) unaryValue = parsePara(line);
                        value = nand.function(value, unaryValue);
                    } else if (tokens.get(counter).getName().equals("LP")) {
                        counter++;
                        unaryValue = parsePara(line);
                        value = nand.function(value, unaryValue);
                    } else value = nand.function(value, symbolToBool(tokens.get(counter).getValue(), line));
                } else if (tokens.get(counter).getValue().equals("AND")) {
                    counter++;
                    if (tokens.get(counter).getValue().equals("Gates.NotGate")) {
                        counter++;
                        if (tokens.get(counter).getName().equals("SYMBOL"))
                            unaryValue = not.function(symbolToBool(tokens.get(counter).getValue(), line));
                        else if (tokens.get(counter).getName().equals("LP")) {
                            unaryValue = parsePara(line);
                        }
                        value = and.function(value, unaryValue);
                    } else if (tokens.get(counter).getName().equals("LP")) {
                        counter++;
                        unaryValue = parsePara(line);
                        value = and.function(value, unaryValue);
                    } else value = and.function(value, symbolToBool(tokens.get(counter).getValue(), line));
                } else if (tokens.get(counter).getValue().equals("Gates.NorGate")) {
                    counter++;
                    if (tokens.get(counter).getValue().equals("Gates.NotGate")) {
                        counter++;
                        if (tokens.get(counter).getName().equals("SYMBOL"))
                            unaryValue = not.function(symbolToBool(tokens.get(counter).getValue(), line));
                        else if (tokens.get(counter).getName().equals("LP")) unaryValue = parsePara(line);
                        value = nor.function(value, unaryValue);
                    } else if (tokens.get(counter).getName().equals("LP")) {
                        counter++;
                        unaryValue = parsePara(line);
                        value = nor.function(value, unaryValue);
                    } else value = nor.function(value, symbolToBool(tokens.get(counter).getValue(), line));
                } else if (tokens.get(counter).getValue().equals("Gates.OrGate")) {
                    counter++;
                    if (tokens.get(counter).getValue().equals("Gates.NotGate")) {
                        counter++;
                        if (tokens.get(counter).getName().equals("SYMBOL"))
                            unaryValue = not.function(symbolToBool(tokens.get(counter).getValue(), line));
                        else if (tokens.get(counter).getName().equals("LP")) unaryValue = parsePara(line);
                        value = or.function(value, unaryValue);
                    } else if (tokens.get(counter).getName().equals("LP")) {
                        counter++;
                        unaryValue = parsePara(line);
                        value = or.function(value, unaryValue);
                    } else value = or.function(value, symbolToBool(tokens.get(counter).getValue(), line));
                } else if (tokens.get(counter).getValue().equals("Gates.XorGate")) {
                    counter++;
                    if (tokens.get(counter).getValue().equals("Gates.NotGate")) {
                        counter++;
                        if (tokens.get(counter).getName().equals("SYMBOL"))
                            unaryValue = not.function(symbolToBool(tokens.get(counter).getValue(), line));
                        else if (tokens.get(counter).getName().equals("LP")) unaryValue = parsePara(line);
                        value = xor.function(value, unaryValue);
                    } else if (tokens.get(counter).getName().equals("LP")) {
                        counter++;
                        unaryValue = parsePara(line);
                        value = xor.function(value, unaryValue);
                    } else value = xor.function(value, symbolToBool(tokens.get(counter).getValue(), line));
                } else if (tokens.get(counter).getValue().equals("Gates.NotGate")) {
                    counter++;
                    if (tokens.get(counter).getName().equals("SYMBOL"))
                        value = not.function(symbolToBool(tokens.get(counter).getValue(), line));
                    else if (tokens.get(counter).getName().equals("LP")) value = parsePara(line);
                }
            } else if (tokens.get(counter).getName().equals("LP")) {
                counter++;
                value = parsePara(line);
            }
            counter++;
        }
        return value;
    }

    public boolean parsePara(int line) {
        boolean value = false;
        boolean unaryValue = false;
        while (counter < tokens.size()) {
            if (tokens.get(counter).getName().equals("SYMBOL"))
                value = symbolToBool(tokens.get(counter).getValue(), line);
            else if (tokens.get(counter).getName().equals("GATE")) {
                if (tokens.get(counter).getValue().equals("Gates.NandGate")) {
                    counter++;
                    if (tokens.get(counter).getValue().equals("Gates.NotGate")) {
                        counter++;
                        if (tokens.get(counter).getName().equals("SYMBOL"))
                            unaryValue = not.function(symbolToBool(tokens.get(counter).getValue(), line));
                        else if (tokens.get(counter).getName().equals("LP")) unaryValue = parsePara(line);
                        value = nand.function(value, unaryValue);
                    } else if (tokens.get(counter).getName().equals("LP")) {
                        counter++;
                        unaryValue = parsePara(line);
                        value = nand.function(value, unaryValue);
                    } else value = nand.function(value, symbolToBool(tokens.get(counter).getValue(), line));
                } else if (tokens.get(counter).getValue().equals("AND")) {
                    counter++;
                    if (tokens.get(counter).getValue().equals("Gates.NotGate")) {
                        counter++;
                        if (tokens.get(counter).getName().equals("SYMBOL"))
                            unaryValue = not.function(symbolToBool(tokens.get(counter).getValue(), line));
                        else if (tokens.get(counter).getName().equals("LP")) unaryValue = parsePara(line);
                        value = and.function(value, unaryValue);
                    } else if (tokens.get(counter).getName().equals("LP")) {
                        counter++;
                        unaryValue = parsePara(line);
                        value = and.function(value, unaryValue);
                    } else value = and.function(value, symbolToBool(tokens.get(counter).getValue(), line));
                } else if (tokens.get(counter).getValue().equals("Gates.NorGate")) {
                    counter++;
                    if (tokens.get(counter).getValue().equals("Gates.NotGate")) {
                        counter++;
                        if (tokens.get(counter).getName().equals("SYMBOL"))
                            unaryValue = not.function(symbolToBool(tokens.get(counter).getValue(), line));
                        else if (tokens.get(counter).getName().equals("LP")) unaryValue = parsePara(line);
                        value = nor.function(value, unaryValue);
                    } else if (tokens.get(counter).getName().equals("LP")) {
                        counter++;
                        unaryValue = parsePara(line);
                        value = nor.function(value, unaryValue);
                    } else value = nor.function(value, symbolToBool(tokens.get(counter).getValue(), line));
                } else if (tokens.get(counter).getName().equals("LP")) {
                    counter++;
                    unaryValue = parsePara(line);
                    value = nor.function(value, unaryValue);
                } else if (tokens.get(counter).getValue().equals("Gates.OrGate")) {
                    counter++;
                    if (tokens.get(counter).getValue().equals("Gates.NotGate")) {
                        counter++;
                        if (tokens.get(counter).getName().equals("SYMBOL"))
                            unaryValue = not.function(symbolToBool(tokens.get(counter).getValue(), line));
                        else if (tokens.get(counter).getName().equals("LP")) unaryValue = parsePara(line);
                        value = or.function(value, unaryValue);
                    } else if (tokens.get(counter).getName().equals("LP")) {
                        counter++;
                        unaryValue = parsePara(line);
                        value = or.function(value, unaryValue);
                    } else value = or.function(value, symbolToBool(tokens.get(counter).getValue(), line));
                } else if (tokens.get(counter).getName().equals("LP")) {
                    counter++;
                    unaryValue = parsePara(line);
                    value = or.function(value, unaryValue);
                } else if (tokens.get(counter).getValue().equals("Gates.XorGate")) {
                    counter++;
                    if (tokens.get(counter).getValue().equals("Gates.NotGate")) {
                        counter++;
                        if (tokens.get(counter).getName().equals("SYMBOL"))
                            unaryValue = not.function(symbolToBool(tokens.get(counter).getValue(), line));
                        else if (tokens.get(counter).getName().equals("LP")) unaryValue = parsePara(line);
                        value = xor.function(value, unaryValue);
                    } else if (tokens.get(counter).getName().equals("LP")) {
                        counter++;
                        unaryValue = parsePara(line);
                        value = xor.function(value, unaryValue);
                    } else value = xor.function(value, symbolToBool(tokens.get(counter).getValue(), line));
                } else if (tokens.get(counter).getValue().equals("Gates.NotGate")) {
                    counter++;
                    if (tokens.get(counter).getName().equals("SYMBOL"))
                        value = not.function(symbolToBool(tokens.get(counter).getValue(), line));
                    else if (tokens.get(counter).getName().equals("LP")) value = parsePara(line);
                }
            } else if (tokens.get(counter).getName().equals("LP")) {
                value = parsePara(line);
            }
            counter++;
        }
        return value;
    }

    private boolean symbolToBool(String a, int i) {
        boolean value = false;
        for (int j = 0; j < symbolsList.size(); j++) {
            if (a.equals(symbolsList.get(j))) value = table[j][i];
        }
        return value;
    }

    private int parseBoolean(boolean value) {
        int parse = 0;
        if (value) parse = 1;
        return parse;
    }
}
